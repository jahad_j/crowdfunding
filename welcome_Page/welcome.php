<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<title>Home</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="welcome_style.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-bar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-red w3-card w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: black; background-color: #fff;">Home</a>
    <a href="../start_Page/start.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">About Us</a>
    <?php if($_SESSION['idProject'] != 0): ?>
    <a href="../myProject_Page/myProject.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">My Project</a>
    <?php endif; ?>
    <?php if(isset($_SESSION['idUser'])): ?>
    <a href="../donation_Page/donation.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Support a Project</a>
    <a href="../logout.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Log Out</a>
    <?php endif; ?>
  </div>

  <!-- Navbar on small screens -->
  <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large">
    <a href="#" class="w3-bar-item w3-button w3-padding-large" style="color: black; background-color: white;">Home</a>
    <a href="../start_Page/start.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">About Us</a>
    <?php if($_SESSION['idProject'] != 0): ?>
    <a href="../myProject_Page/myProject.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">My Project</a>
    <?php endif; ?>
    <?php if(isset($_SESSION['idUser'])): ?>
    <a href="../donation_Page/donation.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Support a Project</a>
    <a href="../logout.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Log Out</a>
    <?php endif; ?>
  </div>
</div>

<?php if(isset($_SESSION['idUser'])): ?>

<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:128px 16px">
  <h1 class="w3-margin w3-jumbo">WELCOME</h1>
  <a href="#header-title"><button class="login button">Check Projects!</button></a>
</header>
<?php endif; ?>
<?php if(!isset($_SESSION['idUser'])): ?>
<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:128px 16px">
  <h1 class="w3-margin w3-jumbo">WELCOME</h1>
  <form action="" method="post">
    <button type="submit" name="submit" value="login" class="login button">Log In</button>
  </form> 
  <?php 
    if(isset($_POST['submit']))
      header('Location: ../login_Page/login.php');
  ?>
</header>
<?php endif; ?>


<div id="header-title" class="header-title">
  <h1 style="font-size: 48px;">Our Projects</h1>
</div>

<div id="container" class="container">

  <?php include_once "../table_projects.php";?>
  <?php foreach($table_arr as $project): ?>
    <div class="card">
      <p class="card-title"> <?php echo $project["projectName"] ?> </p>
      <p class="card-description"> <?php echo $project["projectDescription"] ?> </p>
      <hr style="height: 4px;">
      <div style="text-align: center;">
        <p>Requested Fund -  $<?php echo $project["requestedFund"] ?></p>
        <p>Invested up to Now -  $<?php echo $project["totalInvested"] ?></p>
        <p>Progress:</p> 
      </div>
      <div class="container-progress">
        <div class="skills-progress"><?php echo $project["progress"] ?>  %</div>
      </div>
      <hr style="height: 4px;">
      <div class="button-div">
        <p style="font-weight: bold; color: red;">End date: <?php echo $project["projectEndDate"] ?> </p>
        <form action="../project_Page/project.php" method="post">
          <button type="submit" name="idProject" value="<?php echo $project["idProject"] ?>" class="button-project"><span>Check Out!</span></button>
        </form> 
      </div>
    </div>
    <?php 
      if(isset($_POST['submit'])){
        session_start();
        header('Location: ../login_Page/login.php');
      }
    ?>


  <?php endforeach; ?>

</div>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">  
  <div class="w3-xlarge w3-padding-32">
    <a href="link"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-instagram w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-snapchat w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-pinterest-p w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-twitter w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-linkedin w3-hover-opacity"></i></a>
 </div>
</footer>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>

</body>
</html>
