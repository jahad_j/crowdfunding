<?php 
require_once "../table_projects.php";
session_start();

if (!isset($_POST['idProject']))
	header("location: ../auth_err.html");

$input_id = $_POST['idProject'];

$project_name;
$project_progress;
$total_sum;
$until_now;
foreach ($table_arr as $project) {
	if($project["idProject"] == $input_id){
		$project_name = $project["projectName"];
		$project_progress = $project["progress"];
		$total_sum = $project["requestedFund"];
		$until_now = $project["totalInvested"];
	}
}

