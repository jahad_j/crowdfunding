<?php
include_once('dbConnect.php');

$connect = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);

$query = "SELECT idProject, projectName, projectDescription, projectEndDate, requestedFund, SUM(investmentFund) AS totalInvested FROM projects JOIN projects_investors USING(idProject) GROUP BY idProject ORDER BY idProject ASC";

$result = mysqli_query($connect, $query);


$table_arr = array();
$progress = array();
while ($row_user = mysqli_fetch_assoc($result))
    $table_arr[] = $row_user;
 
foreach ($table_arr as $project => $supportedType) {
	$progress = round(($supportedType["totalInvested"]*100/$supportedType["requestedFund"]));
	$table_arr[$project]["progress"] = $progress;
}
