<?php 
require_once "donation_script.php";
$connect = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $chosenIdProject = mysqli_real_escape_string($connect, $_POST['chosenIdProject']);
    $dueDate = mysqli_real_escape_string($connect, $_POST['duedate']);
    $currencyField = mysqli_real_escape_string($connect, $_POST['currency-field']);

    $project = get_project($table_arr, $chosenIdProject);

	$idUser = $_SESSION['idUser'];
	$sql = "SELECT idUser FROM `projects_investors` WHERE idProject = '$chosenIdProject' AND idUser = '$idUser'";

    $count = mysqli_num_rows(mysqli_query($connect, $sql));

    if(
    	!self_donate($project["idProject"]) &&
    	($count == 0) &&
    	$dueDate < $project["projectEndDate"] &&
    	$currencyField < ($project["requestedFund"] - $project["totalInvested"])
    ){
    	$insert_query = "INSERT INTO projects_investors (idUser, idProject, investmentFund, investmentDate)
						VALUES ('$idUser', '$chosenIdProject', '$currencyField', '$dueDate')";
		mysqli_query($connect, $insert_query);
	    session_start();
        header("location: ../welcome_Page/welcome.php");    	
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="donation_style.css">
<link rel="stylesheet" type="text/css" href="../welcome_Page/welcome_style.css">
<link rel="stylesheet" type="text/css" href="../myProject_Page/myProject_style.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-bar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
</style>


<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-red w3-card w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    <a href="../welcome_Page/welcome.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Home</a>
    <a href="../start_Page/start.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">About Us</a>
    <?php if($_SESSION['idProject'] != 0): ?>
    <a href="../myProject_Page/myProject.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">My Project</a>
    <?php endif; ?>
    <?php if(isset($_SESSION['idUser'])): ?>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: black; background-color: #fff;">Support a Project</a>
    <a href="../logout.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Log Out</a>
    <?php endif; ?>
  </div>

  <!-- Navbar on small screens -->
  <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large">
    <a href="../welcome_Page/welcome.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Home</a>
    <a href="../start_Page/start.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">About Us</a>
    <?php if($_SESSION['idProject'] != 0): ?>
    <a href="../myProject_Page/myProject.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">My Project</a>
    <?php endif; ?>
    <?php if(isset($_SESSION['idUser'])): ?>
    <a href="#" class="w3-bar-item w3-button w3-padding-large" style="color: black; background-color: #fff;">Support a Project</a>
    <a href="../logout.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Log Out</a>
    <?php endif; ?>
  </div>
</div>


<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:128px 16px">
  <h1 class="w3-margin w3-jumbo">Thanks For Your Donation!</h1>
  <a href="#form"><button class="login button">Jump to Donation</button></a>
</header>


<div id="header-title" class="header-title">
  <h1 style="font-size: 48px;">Complete the Form</h1>
</div>

<!-- Form -->
<form id="form" action="" method="post">

	<div class="select-container">
	  <div><h3 style="text-align: center; font-weight: bold;">Select a Project:</h3></div>
	  <select name="chosenIdProject" required>
	    <option style="text-align: center;" value="-1"> ... </option>
	    <?php foreach ($projects as $project): ?>
	    <option value="<?php echo $project["idProject"] ?>"> <?php echo $project["projectName"] ?> </option>
		<?php endforeach; ?>
	  </select>
	</div>
	  <?php
	  if(isset($chosenIdProject)){
	  	if($chosenIdProject == -1)
	  		echo "<p style=\"color: red; text-align: center;\">Please select a project first!</p>";
	  	else if(self_donate($chosenIdProject))
	  		echo "<p style=\"color: red; text-align: center;\">Sorry! Can't donate to your own project</p>";
	  	else if($count != 0)
	  		echo "<p style=\"color: red; text-align: center;\">Sorry! You've already donated to this project</p>";
	  }
	  ?>
	<h1>-</h1>

	<div>
		<label for="duedate">Enter the due date:</label>
		<input type="date" id="duedate" name="duedate" required>
		<?php if($dueDate > $project["projectEndDate"]): ?>
		<p style="color: red; text-align: center;">Sorry! Past the due date.</p>
		<?php endif; ?>
	</div>
	<h1>-</h1>

	<div class="main">
	  	<h1>Amount of Donation</h1>
	    <label for="currency-field">Enter Amount</label>
	    <input type="text" name="currency-field" id="currency-field" data-type="currency" placeholder="$1,000.00" required>
		<?php if($currencyField > ($project["requestedFund"] - $project["totalInvested"])): ?>
		<p style="color: red; text-align: center;">Thanks for being so generous, but your donation exceeds the expected remaining amount</p>
		<?php endif; ?>
	</div>


	<button style="margin-top: 10px;" value="submit" type="submit">Submit</button>

</form>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">  
  <div class="w3-xlarge w3-padding-32">
    <a href="link"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-instagram w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-snapchat w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-pinterest-p w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-twitter w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-linkedin w3-hover-opacity"></i></a>
 </div>
</footer>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>

</body>
</html>
