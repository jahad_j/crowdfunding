<?php 
require_once "../table_projects.php";
include_once('../dbConnect.php');
session_start();

if(!isset($_SESSION['idUser']))
	header("location: ../auth_err.html");

$projects = array();
foreach ($table_arr as $project) {
	$projects[] = $project;
}

function get_project($projects, $idProject){
	$project;
	foreach ($projects as $sample) {
		if($sample["idProject"] == $idProject)
			$project = $sample;
	}

	return $project;
}

function self_donate($chosenIdProject){
	if(isset($_SESSION['idProject']))
		return ($_SESSION['idProject'] == $chosenIdProject);

}