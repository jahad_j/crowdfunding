<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<title>Start Page</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-bar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-red w3-card w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    <a href="../welcome_Page/welcome.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Home</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: black; background-color: white;">About Us</a>
    <?php if($_SESSION['idProject'] != 0): ?>
    <a href="../myProject_Page/myProject.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">My Project</a>
    <?php endif; ?>
    <?php if(isset($_SESSION['idUser'])): ?>
    <a href="../donation_Page/donation.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Support a Project</a>
    <a href="../logout.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Log Out</a>
    <?php endif; ?>
  </div>

  <!-- Navbar on small screens -->
  <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large">
    <a href="../welcome_Page/welcome.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Home</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large" style="color: black; background-color: white;">About Us</a>
    <?php if($_SESSION['idProject'] != 0): ?>
    <a href="../myProject_Page/myProject.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">My Project</a>
    <?php endif; ?>
    <?php if(isset($_SESSION['idUser'])): ?>
    <a href="../donation_Page/donation.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Support a Project</a>
    <a href="../logout.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Log Out</a>
    <?php endif; ?>
  </div>
</div>

<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:128px 16px">
  <h1 class="w3-margin w3-jumbo">About Us</h1>
  <a href="../welcome_Page/welcome.php"><button class="w3-button w3-black w3-padding-large w3-large w3-margin-top">Get Started</button></a>
</header>

<!-- First Grid -->
<div class="w3-row-padding w3-padding-64 w3-container">
  <div class="w3-content">
    <div class="w3-twothird">
      <h1>Crowdfunding - Intro</h1>
      <h5 class="w3-padding-32">Crowdfunding is the practice of funding a project or venture by raising small amounts of money from a large number of people, in modern times typically via the Internet.</h5>

      <p class="w3-text-grey">Using our website you can access numerous startups that can make use of your investments. Our website features many different projects from different parts of the world that mainly focus on advancements in the filed of HI-TECH. We reduce search costs, which allows higher participation in the market. Many individual investors would otherwise have a hard time investing in early-stage companies because of the difficulty of identifying a company directly and the high costs of joining an Angel Group or doing it through a professional venture capital firm.</p>
    </div>

    <div class="w3-third w3-center">
      <i class="fa fa-anchor w3-padding-64 w3-text-red"></i>
    </div>
  </div>
</div>

<!-- Second Grid -->
<div class="w3-row-padding w3-light-grey w3-padding-64 w3-container">
  <div class="w3-content">
    <div class="w3-third w3-center">
      <i class="fa fa-coffee w3-padding-64 w3-text-red w3-margin-right"></i>
    </div>

    <div class="w3-twothird">
      <h1>How Does It Work?</h1>
      <h5 class="w3-padding-32">Modern crowdfunding model is generally based on three types of actors - the project initiator, individuals who support the idea, and a platform (like us) that brings the parties together to launch the idea.</h5>

      <p class="w3-text-grey">Our website is quite simply to use. Click Get Started above to go to the Main page, where you will see the list of projects. On that page you can also Log In. After having logged in you will be able to donate to a project of your choice. Additionally, if you are a project initiator, you will be able to check details of your project using a corresponding link.</p>
    </div>
  </div>
</div>

<div class="w3-container w3-black w3-center w3-opacity w3-padding-64">
    <h1 class="w3-margin w3-xlarge">Quote of the day: live life</h1>
</div>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">  
  <div class="w3-xlarge w3-padding-32">
    <a href="#"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
    <a href="#"><i class="fa fa-instagram w3-hover-opacity"></i></a>
    <a href="#"><i class="fa fa-snapchat w3-hover-opacity"></i></a>
    <a href="#"><i class="fa fa-pinterest-p w3-hover-opacity"></i></a>
    <a href="#"><i class="fa fa-twitter w3-hover-opacity"></i></a>
    <a href="#"><i class="fa fa-linkedin w3-hover-opacity"></i></a>
 </div>
</footer>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>

</body>
</html>
