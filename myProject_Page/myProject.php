<?php require_once "myProject_script.php"; ?>

<!DOCTYPE html>
<html lang="en">
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../welcome_Page/welcome_style.css">
<link rel="stylesheet" type="text/css" href="myProject_style.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-bar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
</style>
<script>
var percent = <?php echo json_encode($myProject["progress"]); ?>;
var remaining_percent = 100 - percent;
var remain_fund = <?php echo json_encode($myProject["remainingFund"]); ?>;
var requestedFund = <?php echo json_encode($myProject["requestedFund"]); ?>;
var untilnow = <?php echo json_encode($myProject["totalInvested"]); ?>;

window.onload = function () {

//Better to construct options first and then pass it as a parameter
var options = {
	animationEnabled: true,
	axisY: {
		tickThickness: 0,
		lineThickness: 0,
		valueFormatString: " ",
		includeZero: true,
		gridThickness: 0                    
	},
	axisX: {
		tickThickness: 0,
		lineThickness: 0,
		labelFontSize: 18,
		labelFontColor: "Black"				
	},
	data: [{
		indexLabelFontSize: 26,
		toolTipContent: "<span style=\"color:black\">{indexLabel}:</span> <span style=\"color:red\"><strong>{y}</strong></span>",
		indexLabelPlacement: "inside",
		indexLabelFontColor: "white",
		indexLabelFontWeight: 600,
		indexLabelFontFamily: "Verdana",
		color: "#f44336",
		type: "bar",
		dataPoints: [
			{ y: percent, label: untilnow + "$", indexLabel: percent + "% Invested" },
			{ y: remaining_percent, label: remain_fund + "$", indexLabel: remaining_percent + "% Left" }
		]
	}]
};

$("#chartContainer").CanvasJSChart(options);
}
</script>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-red w3-card w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    <a href="../welcome_Page/welcome.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Home</a>
    <a href="../start_Page/start.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">About Us</a>
    <?php if($_SESSION['idProject'] != 0): ?>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: black; background-color: #fff;">My Project</a>
    <?php endif; ?>
    <?php if(isset($_SESSION['idUser'])): ?>
    <a href="../donation_Page/donation.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Support a Project</a>
    <a href="../logout.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" style="color: white; background-color: #f44336;">Log Out</a>
    <?php endif; ?>
  </div>

  <!-- Navbar on small screens -->
  <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large">
    <a href="../welcome_Page/welcome.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Home</a>
    <a href="../start_Page/start.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">About Us</a>
    <?php if($_SESSION['idProject'] != 0): ?>
    <a href="../myProject_Page/myProject.php" class="w3-bar-item w3-button w3-padding-large" style="color: black; background-color: #fff;">My Project</a>
    <?php endif; ?>
    <?php if(isset($_SESSION['idUser'])): ?>
    <a href="../donation_Page/donation.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Support a Project</a>
    <a href="../logout.php" class="w3-bar-item w3-button w3-padding-large" style="color: white; background-color: #f44336;">Log Out</a>
    <?php endif; ?>
  </div>
</div>


<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:128px 16px">
  <h1 class="w3-margin w3-jumbo"><?php echo $myProject["projectName"] ?></h1>
  <a href="#header-title"><button class="login button">See Below...</button></a>
</header>


<div id="header-title" class="header-title">
  <h1 style="font-size: 48px;">Infographics</h1>
</div>

<main>

<div class="chartContainer" id="chartContainer"></div>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<table class="styled-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Donated</th>
            <th>Donation Date</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($donors as $donor): ?>
        <tr>
            <td><?php echo $donor["idUser"] ?></td>
            <td><?php echo $donor["name"] ?></td>
            <td>$ <?php echo $donor["investmentFund"] ?></td>
            <td><?php echo $donor["investmentDate"] ?></td>
        </tr>
    	<?php endforeach; ?>
    </tbody>
</table>

</main>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">  
  <div class="w3-xlarge w3-padding-32">
    <a href="link"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-instagram w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-snapchat w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-pinterest-p w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-twitter w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-linkedin w3-hover-opacity"></i></a>
 </div>
</footer>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>

</body>
</html>
