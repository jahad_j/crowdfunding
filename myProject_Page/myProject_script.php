<?php 
require_once "../table_projects.php";
include_once('../dbConnect.php');
session_start();

if(!isset($_SESSION['idUser']) || ($_SESSION['idProject'] == 0))
	header("location: ../auth_err.html");

if ($_SESSION['idProject'] == 0)
	echo "Bad!";

$project_id = $_SESSION['idProject'];

$myProject = array();
foreach ($table_arr as $project) {
	if($project["idProject"] == $_SESSION['idProject']){
		$myProject["projectName"] = $project["projectName"];
		$myProject["progress"] = $project["progress"];
		$myProject["requestedFund"] = $project["requestedFund"];
		$myProject["totalInvested"] = $project["totalInvested"];
		$myProject["remainingFund"] = $project["requestedFund"] - $project["totalInvested"];
		$myProject["projectDescription"] = $project["projectDescription"];
		$myProject["projectEndDate"] = $project["projectEndDate"];
	}
}

$donors = array();
$sql = "SELECT * FROM projects_investors WHERE idProject = '$project_id' ORDER BY investmentDate DESC";
$result = mysqli_query($connect, $sql);
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
	$donors_dup["idUser"] = $row["idUser"];
	$donors_dup["name"] = get_name($connect, $row["idUser"]);
	$donors_dup["idProject"] = $row["idProject"];
	$donors_dup["investmentFund"] = $row["investmentFund"];
	$donors_dup["investmentDate"] = $row["investmentDate"];
	$donors[] = $donors_dup;
}

function get_name($connect, $idUser){
	$sql = "SELECT CONCAT(firstname, ' ', lastname) AS name FROM users WHERE idUser = '$idUser'";
	$result = mysqli_query($connect, $sql);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

	return $row["name"];
}


