<?php
include_once('../dbConnect.php');

$connect = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);

// for test purposes
// dcornhill1@salon.com
// NT0paN
   
if($_SERVER["REQUEST_METHOD"] == "POST") {
   // username and password sent from form 
         
   $email = mysqli_real_escape_string($connect, $_POST['email']);
   $password = mysqli_real_escape_string($connect, $_POST['password']);
         
   $sql = "SELECT idUser FROM users WHERE email = '$email' and password = '$password'";
   $result = mysqli_query($connect, $sql);
   $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
   $active = $row['active'];
         
   $count = mysqli_num_rows($result);
         
  // If result matched $e_mail and $password, table row must be 1 row
  
  if($count == 1) { 
	    session_start();
      $_SESSION['idUser'] = $row["idUser"];
      $_SESSION['idProject'] = is_projectowner($connect, $_SESSION['idUser']);
      header("location: ../welcome_Page/welcome.php");
  } else 
      $count = -1;
}
mysqli_close($connect);


// if proj own ret proj id, else ret 0
function is_projectowner($connect, $idUser){
    $sql = "SELECT idProject FROM projects WHERE idUser = '$idUser'";
    $result = mysqli_query($connect, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $active = $row['active'];

    $count = mysqli_num_rows($result);
    if($count == 1)
      return $row['idProject'];

    return 0;
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Log In Page</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="login_style.css">
</head>
<body>

<form class="form" action="" method="post">
    <div class="title">Welcome</div>
    <div class="subtitle">Enter your account!</div>
    <div class="input-container ic1">
      <input id="email" class="input" name="email" type="text" placeholder=" " />
      <div class="cut cut-short"></div>
      <label for="email" class="placeholder">Email</label>
    </div>
    <div class="input-container ic2">
      <input id="password" class="input" name="password" type="password" placeholder=" " />
      <div class="cut"></div>
      <label for="password" class="placeholder">Password</label>
    </div>
    <?php if($count == -1): ?>
    <div class="input-container ic3">
      <p>Incorrect email or password!</p>
    </div>
    <?php endif; ?>
    <button type="text" value="submit" class="submit">LOG IN</button>
</form>
</body>
</html>